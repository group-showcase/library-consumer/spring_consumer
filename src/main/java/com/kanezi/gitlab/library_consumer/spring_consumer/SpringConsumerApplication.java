package com.kanezi.gitlab.library_consumer.spring_consumer;

import com.kanezi.gitlab.library_consumer.uuid_timestamp.UuidTimestampGenerator;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
public class SpringConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringConsumerApplication.class, args);
	}

}


@RestController
@RequestMapping("/")
class TimestampUUIDController {

	@GetMapping
	String rand() {
		return UuidTimestampGenerator.generate();
	}

}